﻿
namespace TicTacToe
{
    public class CheckWins
    {
        public bool CheckVertical(int i, string[,] playingTab, int winSize, string x, string o, int size)
        {
            int countX = 0;
            int countY = 0;
            for (int j = 1; j < size; j++)
            {
                if (playingTab[i, j] == x)
                {
                    countX++;
                    if (countX == winSize)
                    {
                        return true;
                    }
                }
                else countX = 0;

                if (playingTab[i, j] == o)
                {
                    countY++;
                    if (countY == winSize)
                        return true;
                }
                else countY = 0;
            }
            return false;
        }
        public bool CheckHorizontal(int j, string[,] playingTab, int winSize, string x, string o, int size)
        {
            int countX = 0;
            int countY = 0;
            for (int i = 1; i < size; i++)
            {
                if (playingTab[i, j] == x)
                {
                    countX++;
                    if (countX == winSize)
                    {
                        return true;
                    }
                }
                else countX = 0;

                if (playingTab[i, j] == o)
                {
                    countY++;
                    if (countY == winSize)
                    {
                        return true;
                    }
                }
                else countY = 0;
            }
            return false;
        }


        public bool CheckLeftDiagonal(string[,] playingTab, int winSize, string x, string o, int size)
        {
            int countX = 0;
            int countY = 0;
            for (int j = 1; j < size; j++)
            {
                for (int i = 1; i < size; i++)
                {
                    if (playingTab[i, j] == x)
                    {
                        for (int m = 1; m < winSize; m++)
                        {
                            if (i + m < size && j + m < size)
                            {
                                if (playingTab[i + m, j + m] == x)
                                {
                                    countX++;

                                    if (countX == winSize - 1)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                        countX = 0;
                    }
                }
            }

            for (int j = 1; j < size; j++)
            {
                for (int i = 1; i < size; i++)
                {
                    if (playingTab[i, j] == o)
                    {
                        for (int m = 1; m < winSize; m++)
                        {
                            if (i + m < size && j + m < size)
                            {
                                if (playingTab[i + m, j + m] == o)
                                {
                                    countY++;
                                    if (countY == winSize - 1)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                        countY = 0;
                    }
                }
            }
            return false;
        }

        public bool CheckRightDiagonal(string[,] playingTab, int winSize, string x, string o, int size)
        {
            int countX = 0;
            int countY = 0;
            for (int j = size - 1; j > 0; j--)
            {
                for (int i = 1; i < size; i++)
                {
                    if (playingTab[i, j] == x)
                    {
                        for (int m = 1; m < winSize; m++)
                        {
                            if (i + m < size && j - m > 0)
                            {
                                if (playingTab[i + m, j - m] == x)
                                {
                                    countX++;
                                    if (countX == winSize - 1)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                        countX = 0;
                    }
                }
            }

            for (int j = size - 1; j > 0; j--)
            {
                for (int i = 1; i < size; i++)
                {
                    if (playingTab[i, j] == o)
                    {
                        for (int m = 1; m < winSize; m++)
                        {
                            if (i + m < size && j - m > 0)
                            {
                                if (playingTab[i + m, j - m] == o)
                                {
                                    countY++;
                                    if (countY == winSize - 1)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }

                        countY = 0;
                    }
                }
            }
            return false;
        }
    }
}
