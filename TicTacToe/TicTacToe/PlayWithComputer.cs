﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToe
{
    public class PlayWithComputer
    {
        //Gdy komputer wybiera ruch robi to w takiej kolejności : 

        // - sprawdza czy  użytkownik nie ma ciągu znaków o 2 mniejszego niż wygrywający w pionie, 
        //jeśli tak to blokuje go z dołu, jeśli nie może blokuje go z góry
        // - sprawdza czy  użytkownik nie ma ciągu znaków o 2 mniejszego niż wygrywający w poziomie, 
        //jeśli tak to blokuje go z prawej, jeśli nie może blokuje go z lewej

        // - jeśli nie ma ruchów zagrażających wybiera pole obok swojego pod dołem, jeśli nie może - na górze
        // - jeśli nie może ani na dole ani na górze wybiera prawą lub lewą stronę
        // (W przypadku gry 3x3 komputer od razu blokuje użytkownika)

        public void PlayGame()
        {
            var boardGenerate = new BoardGenerate();
            var checkWins = new CheckWins();

            var size = boardGenerate.GetBoardSize();
            var winSize = boardGenerate.GetWinSize(size);
            var tab = boardGenerate.GenerateBoard(size);
            string[,] playingTab = new string[size, size];
            boardGenerate.GenerateBlankBoard(size);

            string player = "1";
            bool work2 = true;

            for (int j = 0; j < size; j++)
            {
                for (int i = 0; i < size; i++)
                {
                    playingTab[i, j] = "   ";
                }
            }

            while (work2)
            {
                string field;
                if (player == "1")
                {
                    field = GetField();
                }
                else
                {
                    field = GetComputerField(size, winSize, playingTab);
                }
                Console.Clear();

                char[] charTab = "ABCDEFGHIJKLMNOPRSTUWXYZ".ToCharArray();

                bool work = true;
                bool msg = true;

                while (work)
                {
                    for (int j = 0; j < size - 1; j++)
                    {
                        for (int i = 0; i < size - 1; i++)
                        {
                            playingTab[0, 0] = "  ";
                            playingTab[i + 1, 0] = $" {i} ";
                            playingTab[0, j + 1] = $" {charTab[j]}";

                            if (i > 0 && j > 0)
                            {
                                if (tab[i, j] == field)
                                {
                                    if (playingTab[i, j] == " X " || playingTab[i, j] == " 0 ")
                                    {
                                        msg = false;
                                    }

                                    else
                                    {
                                        if (player == "1")
                                        {
                                            playingTab[i, j] = " X ";
                                            player = "2";
                                        }
                                        else
                                        {
                                            playingTab[i, j] = " 0 ";
                                            player = "1";
                                        }
                                    }
                                }
                            }
                            Console.Write(playingTab[i, j]);
                        }
                        Console.WriteLine("\n");

                        work = false;
                    }

                    for (int i = 1; i < size; i++)
                    {
                        if (checkWins.CheckVertical(i, playingTab, winSize, " X ", " 0 ", size))
                        {
                            PrintWinMsg(player);
                            work2 = false;
                        }
                    }
                    for (int j = 1; j < size; j++)
                    {
                        if (checkWins.CheckHorizontal(j, playingTab, winSize, " X ", " 0 ", size))
                        {
                            PrintWinMsg(player);
                            work2 = false;
                        }
                    }

                    if (checkWins.CheckLeftDiagonal(playingTab, winSize, " X ", " 0 ", size) ||
                        checkWins.CheckRightDiagonal(playingTab, winSize, " X ", " 0 ", size))
                    {
                        PrintWinMsg(player);
                        work2 = false;
                    }

                    int drawCount = 0;
                    for (int j = 1; j < size; j++)
                    {
                        for (int i = 1; i < size; i++)
                        {
                            if (playingTab[i, j] == " X " || playingTab[i, j] == " 0 ")
                            {
                                var dupa = playingTab[i, j].ToList();
                                drawCount = drawCount + dupa.Count;

                                if (drawCount == (size - 2) * (size - 2) * 3)
                                {
                                    Console.WriteLine("Draw !!");
                                    work2 = false;
                                }
                            }
                        }
                    }

                    if (msg == false)
                        Console.WriteLine("The field has been taken");
                }
            }
        }

        private string GetComputerField(int size, int winsize, string[,] playingTab)
        {
            char[] charTab = "ABCDEFGHIJKLMNOPRSTUWXYZ".ToCharArray();
            Random random = new Random();
            var number = random.Next(0, size - 2);
            var letter = charTab[random.Next(0, size - 2)];

            
            var checkList = CheckAgainstVertical(size, playingTab, winsize);
            if (checkList != null)
            {

                if (playingTab[checkList[0] + 1, checkList[1] + 1] != " X " &&
                    playingTab[checkList[0] + 1, checkList[1] + 1] != " 0 ")
                {
                    letter = charTab[checkList[1]];
                    number = checkList[0];
                }
            }

            var field = $"{letter}{number}";
            return field;
        }

        public void PrintWinMsg(string player)
        {
            Console.WriteLine($"The player {player} has won !!");
        }

        private string GetField()
        {
            Console.WriteLine("Get the field: ");
            string field = Console.ReadLine();
            return field;
        }

        public List<int> CheckAgainstVertical(int size, string[,] playingTab, int winSize)
        {
            var coordinates = new List<int>();
            var numberOfOccurrences = winSize - 2;
            if (numberOfOccurrences == 1)
            {
                numberOfOccurrences = 2;
            }
            for (int i = 1; i < size; i++)
            {
                int count = 0;

                for (int j = 1; j < size; j++)
                {
                    if (playingTab[i, j] == " X ")
                    {
                        count++;

                        if (count == numberOfOccurrences)
                        {
                            if (j + 2 < size)
                            {
                                if (playingTab[i, j + 1] != " 0 ")
                                {
                                    coordinates.Add(i - 1);
                                    coordinates.Add(j);
                                    return coordinates;
                                }
                            }
                            else
                            {
                                if (playingTab[i, j - numberOfOccurrences] != " 0 ")
                                {
                                    coordinates.Add(i - 1);
                                    coordinates.Add(j - numberOfOccurrences - 1);

                                    return coordinates;
                                }
                            }
                        }
                    }
                    else count = 0;
                }
            }
            return CheckAgainstHorizontal(playingTab, winSize, size);
        }

        public List<int> CheckAgainstHorizontal(string[,] playingTab, int winSize, int size)
        {
            var coordinates = new List<int>();

            var numberOfOccurrences = winSize - 2;
            if (numberOfOccurrences == 1)
            {
                numberOfOccurrences = 2;
            }
            for (int j = 1; j < size; j++)
            {
                int count = 0;

                for (int i = 1; i < size; i++)
                {
                    if (playingTab[i, j] == " X ")
                    {
                        count++;
                        if (count == numberOfOccurrences)
                        {
                            if (i + 2 < size)
                            {
                                if (playingTab[i + 1, j + 1] != " 0 ")
                                {
                                    coordinates.Add(i);
                                    coordinates.Add(j - 1);
                                    return coordinates;
                                }
                            }
                            else
                            {
                                if (playingTab[i - numberOfOccurrences, j] != " 0 ")
                                {
                                    coordinates.Add(i - numberOfOccurrences - 1);
                                    coordinates.Add(j - 1);
                                    return coordinates;
                                }
                            }
                        }
                    }
                    else count = 0;
                }

            }
            return ComputerHorizontalAttack(playingTab, winSize, size);
        }

        public List<int> ComputerVerticalAttack(string[,] playingTab, int winSize, int size)
        {
            var coordinates = new List<int>();
            var numberOfOccurrences = 1;

            for (int i = 1; i < size; i++)
            {
                for (int j = 1; j < size; j++)
                {
                    if (playingTab[i, j] == " 0 ")
                    {
                        if (j + 2 < size)
                        {
                            if (playingTab[i, j + 1] != " 0 ")
                            {
                                coordinates.Add(i - 1);
                                coordinates.Add(j);
                                return coordinates;
                            }
                        }
                        else
                        {
                            if (playingTab[i, j - numberOfOccurrences] != " 0 ")
                            {
                                coordinates.Add(i - 1);
                                coordinates.Add(j - numberOfOccurrences);

                                return coordinates;
                            }
                        }
                    }
                }
            }
            return null;
        }
        public List<int> ComputerHorizontalAttack(string[,] playingTab, int winSize, int size)
        {
            var coordinates = new List<int>();

            var numberOfOccurrences = 2;
            
            for (int j = 1; j < size; j++)
            {
                int count = 0;

                for (int i = 1; i < size; i++)
                {
                    if (playingTab[i, j] == " 0 ")
                    {
                        count++;
                        if (count == numberOfOccurrences)
                        {
                            if (i + 2 < size)
                            {
                                if (playingTab[i + 1, j + 1] != " 0 ")
                                {
                                    coordinates.Add(i);
                                    coordinates.Add(j - 1);
                                    return coordinates;
                                }
                            }
                            else
                            {
                                if (playingTab[i - numberOfOccurrences, j] != " 0 ")
                                {
                                    coordinates.Add(i - numberOfOccurrences - 1);
                                    coordinates.Add(j - 1);
                                    return coordinates;
                                }
                            }
                        }
                    }
                    else count = 0;
                }

            }
            return ComputerVerticalAttack(playingTab,winSize,size);
        }
    }
}