﻿
using System;

namespace TicTacToe
{
    class Program
    {
        static void Main()
        {
            
                bool work = true;
                while (work)
                {
                    Console.WriteLine("Choose game: \n1 - Two players\n2 - Play with computer\n3 - Exit");
                    string choose = Console.ReadLine();
                    switch (choose)
                    {
                        case "1":
                            new Play().PlayGame();
                            break;
                        case "2":
                            new PlayWithComputer().PlayGame();
                            break;
                        case "3":
                            work = false;
                            break;
                        default:
                            Console.WriteLine("Command not found");
                            break;
                    }
                }
            
        }
    }
}
