﻿using System;
using System.Linq;

namespace TicTacToe
{
    public class Play
    {
        public void PlayGame()
        {

            var boardGenerate = new BoardGenerate();
            var checkWins = new CheckWins();

            var size = boardGenerate.GetBoardSize();
            var winSize = boardGenerate.GetWinSize(size);
            var tab = boardGenerate.GenerateBoard(size);
            string[,] playingTab = new string[size, size];
            boardGenerate.GenerateBlankBoard(size);

            string player = "1";
            bool work2 = true;

            for (int j = 0; j < size; j++)
            {
                for (int i = 0; i < size; i++)
                {
                    playingTab[i, j] = "   ";
                }
            }

            while (work2)
            {
                var field = GetField();
                Console.Clear();

                char[] charTab = "ABCDEFGHIJKLMNOPRSTUWXYZ".ToCharArray();

                bool work = true;
                bool msg = true;

                while (work)
                {
                    for (int j = 0; j < size - 1; j++)
                    {
                        for (int i = 0; i < size - 1; i++)
                        {
                            playingTab[0, 0] = "  ";
                            playingTab[i + 1, 0] = $" {i} ";
                            playingTab[0, j + 1] = $" {charTab[j]}";

                            if (i > 0 && j > 0)
                            {
                                if (tab[i, j] == field)
                                {
                                    if (playingTab[i, j] == " X " || playingTab[i, j] == " 0 ")
                                    {
                                        msg = false;
                                    }
                                    else
                                    {
                                        if (player == "1")
                                        {
                                            playingTab[i, j] = " X ";
                                            player = "2";
                                        }
                                        else
                                        {
                                            playingTab[i, j] = " 0 ";
                                            player = "1";
                                        }
                                    }
                                }
                            }
                            Console.Write(playingTab[i, j]);
                        }
                        Console.WriteLine("\n");

                        work = false;
                    }

                    for (int i = 1; i < size; i++)
                    {
                        if (checkWins.CheckVertical(i, playingTab, winSize, " X ", " 0 ", size))
                        {
                            PrintWinMsg(player);
                            work2 = false;
                        }
                    }
                    for (int j = 1; j < size; j++)
                    {
                        if (checkWins.CheckHorizontal(j, playingTab, winSize, " X ", " 0 ", size))
                        {
                            PrintWinMsg(player);
                            work2 = false;
                        }
                    }

                    if (checkWins.CheckLeftDiagonal(playingTab, winSize, " X ", " 0 ", size) ||
                        checkWins.CheckRightDiagonal(playingTab, winSize, " X ", " 0 ", size))
                    {
                        PrintWinMsg(player);
                        work2 = false;
                    }

                    int drawCount = 0;
                    for (int j = 1; j < size; j++)
                    {
                        for (int i = 1; i < size; i++)
                        {
                            if (playingTab[i, j] == " X " || playingTab[i, j] == " 0 ")
                            {
                                var dupa = playingTab[i, j].ToList();
                                drawCount = drawCount + dupa.Count;

                                if (drawCount == (size - 2) * (size - 2) * 3)
                                {
                                    Console.WriteLine("Draw !!");
                                    work2 = false;
                                }
                            }
                        }
                    }

                    if (msg == false)
                        Console.WriteLine("The field has been taken");
                }
            }
        }

        public void PrintWinMsg(string player)
        {
            Console.WriteLine($"The player {player} has won !!");
        }

        private string GetField()
        {
            Console.WriteLine("Get the fild: ");
            string field = Console.ReadLine();
            return field;
        }
    }
}
