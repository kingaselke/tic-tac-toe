﻿using System;

namespace TicTacToe
{
    public class BoardGenerate
    {
        public string Player;
        public bool Signs = true;

        public int GetBoardSize()
        {
            int size = GetInt($"Get the board size: ");

            while (size <= 2 || size > 10)
            {
                size = GetInt($"The size should be between 3 and 10: ");
            }

            size = size + 2;
            return size;
        }

        public int GetWinSize(int size)
        {
            int winSize = GetInt("Get the string lenght to win: ");
            while (winSize <= 2 || winSize > 10)
            {

                winSize = GetInt("The win size should be between 3 and 10 and smaller than board size ! ");
            }
            while (winSize > size - 2)
            {
                winSize = GetInt("The win size should be between 3 and 10 and smaller than board size ! ");
            }
            return winSize;
        }

        public int GetInt(string msg)
        {
            while (true)
            {
                Console.WriteLine(msg);
                int value;
                string input = Console.ReadLine();
                if (int.TryParse(input, out value))
                    return value;
                if (string.IsNullOrEmpty(input))
                    return 0;
            }
        }

        public string[,] GenerateBoard(int size)
        {
            char[] charTab = "ABCDEFGHIJKLMNOPRSTUWXYZ".ToCharArray();

            string[,] tab = new string[size, size];

            for (int j = 0; j < size - 1; j++)
            {
                for (int i = 0; i < size - 1; i++)
                {
                    tab[0, 0] = "   ";
                    tab[i + 1, 0] = $"{i}";
                    tab[0, j + 1] = $"{charTab[j]}";

                    tab[i + 1, j + 1] = $"{charTab[j]}{i}";
                }
            }
            return tab;
        }

        public string[,] GenerateBlankBoard(int size)
        {
            char[] charTab = "ABCDEFGHIJKLMNOPRSTUWXYZ".ToCharArray();

            string[,] blankTab = new string[size, size];
            for (int j = 0; j < size - 1; j++)
            {
                for (int i = 0; i < size - 1; i++)
                {
                    blankTab[0, 0] = "   ";
                    blankTab[i + 1, 0] = $" {i} ";
                    blankTab[0, j + 1] = $" {charTab[j]} ";

                    Console.Write(blankTab[i, j]);
                }
                Console.WriteLine("\n");
            }
            return blankTab;
        }
    }
}










